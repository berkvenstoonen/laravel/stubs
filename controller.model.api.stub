<?php
declare(strict_types=1);

namespace {{ namespace }};

use {{ namespacedModel }};
use {{ rootNamespace }}Http\Controllers\Controller;
use {{ namespacedRequests }}
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

final class {{ class }} extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('{{ modelVariable }}.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store({{ storeRequest }} $request): RedirectResponse
    {
        ${{ modelVariable }} = {{ model }}::create($request->validationData());
        return redirect()->route('{{ modelVariable }}.show', [${{ modelVariable }}]);
    }

    /**
     * Display the specified resource.
     */
    public function show({{ model }} ${{ modelVariable }}): View
    {
        return view('{{ modelVariable }}.view', compact('{{ modelVariable }}'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update({{ updateRequest }} $request, {{ model }} ${{ modelVariable }}): RedirectResponse
    {
        ${{ modelVariable }}->updateOrFail($request->validationData());
        return redirect()->route('{{ modelVariable }}.show', [${{ modelVariable }}]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy({{ model }} ${{ modelVariable }}): RedirectResponse
    {
        ${{ modelVariable }}->deleteOrFail();
        return redirect()->route('{{ modelVariable }}.index');
    }
}
